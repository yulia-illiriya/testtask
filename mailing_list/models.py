from django.db import models
from django.utils import timezone

# Сущность "рассылка" имеет атрибуты:
# уникальный id рассылки
# дата и время запуска рассылки
# текст сообщения для доставки клиенту
# фильтр свойств клиентов, на которых должна быть произведена рассылка (код мобильного оператора, тег)
# дата и время окончания рассылки: если по каким-то причинам не успели разослать все сообщения - никакие сообщения клиентам после этого времени доставляться не должны


# Сущность "сообщение" имеет атрибуты:

# уникальный id сообщения
# дата и время создания (отправки)
# статус отправки
# id рассылки, в рамках которой было отправлено сообщение
# id клиента, которому отправили

class MailingList(models.Model):
    start_at = models.DateTimeField(verbose_name='Запустить рассылку в', default=timezone.now)
    message_text = models.TextField(verbose_name='Текст сообщения')
    filter_mobile_operator = models.CharField(max_length=10, verbose_name='Код мобильного оператора', blank=True, null=True)
    filter_tag = models.CharField(max_length=120, verbose_name='Тег', blank=True, null=True)
    end_time = models.DateTimeField(verbose_name='Дата и время окончания рассылки', default=timezone.now() + timezone.timedelta(hours=1))

    def __str__(self):
        return f'Рассылка {self.id} ({self.start_at})'


class Message(models.Model):
    """
    Несмотря на ТЗ, это, скорее, не модель сообщения, а детали сообщения, промежуточная модель
    для таблиц Client и MailingList
    """
    
    STATUS_CHOICES = [
        ('sent', 'Отправлено'),
        ('delivered', 'Доставлено'),
        ('read', 'Прочитано'),
        ('error', 'Ошибка отправки'),
    ]
    
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Создано в')
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, verbose_name='Статус отправки')
    mailing_list_id = models.ForeignKey(MailingList, on_delete=models.CASCADE, verbose_name='ID рассылки')
    client_id = models.ForeignKey('client.Client', on_delete=models.CASCADE, verbose_name='ID клиента')

    def __str__(self):
        return f'Рассылка {self.mailing_list_id} для клиента {self.client_id} ({self.creation_datetime})'