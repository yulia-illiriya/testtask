import requests
from celery import shared_task
from celery import group
from celery.utils.log import get_task_logger
from django.utils import timezone
from .models import MailingList, Message
from client.models import Client

logger = get_task_logger(__name__)
            
@shared_task
def send_message(jwt_token, mailing_list_id, scheduled_time):
    api_url = "https://probe.fbrq.cloud/v1/send/"
    
    headers = {
        "Authorization": f"Bearer {jwt_token}",
        "Content-Type": "application/json",
    }

    mailing_list = MailingList.objects.get(id=mailing_list_id)
    clients = Client.objects.filter(message__mailing_list_id=mailing_list_id)
    
    # данные для каждого клиента
    data_list = [
        {
            "msgId": message.pk,
            "phone": client.phone_number,
            "text": mailing_list.message_text,
            "api_url": api_url + str(message.pk) # мы предполагаем, что именно id сообщения в конце (в ТЗ нет уточнения)
        }
        for client in clients
        for message in Message.objects.filter(client_id=client.id)
    ]

    try:
        group(
            send_single_message.s(api_url, headers, data).set(countdown=(scheduled_time - timezone.now()).total_seconds())
            for data in data_list
        ).apply_async(eta=scheduled_time) # можно настроить через celery-beat периодическую проверку и запуск
        
    except Exception as e:
        print(f"An error occurred: {e}")

@shared_task
def send_single_message(api_url, headers, data):
    try:
        response = requests.post(api_url, json=data, headers=headers)

        if response.status_code == 200:
            result = response.json()
            print(f"API response: {result}")
            message = Message.objects.get(pk=data['msgId'])
            message.status = 'sent'
            message.save()
        else:
            print(f"Error {response.status_code}: {response.text}")
            message = Message.objects.get(pk=data['msgId'])
            message.status = 'error'
            message.save()
    except Exception as e:
        print(f"An error occurred: {e}")