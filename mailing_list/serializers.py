from rest_framework import serializers
from django.utils import timezone

from .models import MailingList, Message

class MailingListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailingList
        fields = ['id', 'start_at', 'end_time', 'message_text', 'filter_mobile_operator', 'filter_tag']
        
    def validate_start_at(self, start):
        if start < timezone.now():
            raise serializers.ValidationError('Рассылка не может стартовать раньше текущего момента')
        return start
    
    def validate_end_time(self, end):
        if end < timezone.now():
            raise serializers.ValidationError('Рассылка не может закончиться раньше текущего момента')
        return end


class MessageSerializer(serializers.ModelSerializer):
    client_phone_number = serializers.SerializerMethodField()
    
    class Meta:
        model = Message
        fields = '__all__'
        
    def get_client_phone_number(self, obj):
        return obj.client_id.phone_number
