from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import (
    MailingListViewSet, 
    MessageViewSet,
    StatisticsView,
    CreateMailingListAndMessages,
    MailingListStatisticsView
)

router = DefaultRouter()
router.register(r'mailing-lists', MailingListViewSet, basename='mailing-list')
router.register(r'messages', MessageViewSet, basename='message')

urlpatterns = [
    path('mailing-lists/', MailingListViewSet.as_view({'get': 'list', 'post': 'create', 'delete': 'destroy'})),
    path('messages/', MessageViewSet.as_view({'get': 'list', 'post': 'create', 'delete': 'destroy'})),
    path('create-mailing-list-with-message/', CreateMailingListAndMessages.as_view()),
    path('stats/', StatisticsView.as_view()),
    path('message-stats/', MailingListStatisticsView.as_view())
]

urlpatterns += router.urls
