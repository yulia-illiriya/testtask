from datetime import timedelta
from rest_framework import viewsets
from environ import Env
from django.db.models import Count
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from client.models import Client
from .models import MailingList, Message
from .serializers import MailingListSerializer, MessageSerializer
from mailing_list.tasks import send_message

env = Env()
env.read_env("../.env")

JWT = env.str("JWT")

class MailingListViewSet(viewsets.ModelViewSet):
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer
    
class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

class CreateMailingListAndMessages(APIView):
    """
    пример вью для создания рассылки со всеми данными.
    модель сообщения создается автоматически
    """
    
    def post(self, request, *args, **kwargs):
        mailing_list_data = request.data.get('mailing_list', {})
        clients = request.data.get('clients', [])
        scheduled_time = mailing_list_data.get('start_at') + timedelta(seconds=1)

        mailing_list_serializer = MailingListSerializer(data=mailing_list_data)
        mailing_list_serializer.is_valid(raise_exception=True)
        mailing_list = mailing_list_serializer.save()
        
        messages_data = []
        for client_data in clients:
            client_id = client_data.get('id')
            if client_id:
                client = Client.objects.get(pk=client_id)
                message_data = {
                    'status': 'create',
                    'mailing_list': mailing_list.id,
                    'client': client.id,
                }
                messages_data.append(message_data)

        message_serializer = MessageSerializer(data=messages_data, many=True)
        message_serializer.is_valid(raise_exception=True)
        message_serializer.save()

        send_message.delay(JWT, mailing_list.pk, scheduled_time)
 

        return Response(
            {
                'mailing_list': mailing_list_serializer.data,
                'messages': message_serializer.data,
            },
            status=status.HTTP_201_CREATED
        )
        
        
class StatisticsView(APIView):
    """
    здесь просто статистика: 
    столько рассылок, общее число сообщений столько-то,
    в них общее количество сообщений с таким-то статусом.
    если дописать фильтрацию, то будет за определенный период.
    но в ТЗ нет
    """
    
    def get(self, request, *args, **kwargs):
        mailing_lists = MailingList.objects.all()
        mailing_list_serializer = MailingListSerializer(mailing_lists, many=True)
        
        message_stats = Message.objects.values('status').annotate(count=Count('status'))

        # Собираем результат для ответа
        result = {
            'total_messages': Message.objects.count(),
            'total_mailing_lists': MailingList.objects.count(),
            'mailing_lists': mailing_list_serializer.data,
            'message_stats': message_stats,
        }

        return Response(result, status=status.HTTP_200_OK)
        
class MailingListStatisticsView(APIView):
    """
    Для отдельной рассылки
    """
    
    def get(self, request, mailing_list_id, *args, **kwargs):
        try:
            mailing_list = MailingList.objects.get(pk=mailing_list_id)
        except MailingList.DoesNotExist:
            return Response({'error': 'MailingList not found'}, status=status.HTTP_404_NOT_FOUND)

        messages = Message.objects.filter(mailing_list_id=mailing_list_id)
        message_serializer = MessageSerializer(messages, many=True)

        status_counts = messages.values('status').annotate(count=Count('status'))
        
        result = {
            'mailing_list': {
                'id': mailing_list.id,
                'start_at': mailing_list.start_at,
                'end_time': mailing_list.end_time,
                'message_text': mailing_list.message_text,
                'filter_mobile_operator': mailing_list.filter_mobile_operator,
                'filter_tag': mailing_list.filter_tag,
            },
            'message_stats': message_serializer.data,
            'status_counts': status_counts,
        }

        return Response(result, status=status.HTTP_200_OK)