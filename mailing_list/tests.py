import pytest
from datetime import timedelta
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase
from .models import MailingList, Message
from client.models import Client
from .serializers import MailingListSerializer, MessageSerializer

@pytest.mark.django_db
class TestMailingListAPI(APITestCase):

    def test_create_mailing_list(self):
        """
        Проверяем нормальный ввод данных
        """

        url = reverse('mailing-list-list')

        # тестовые данные
        data = {
            'start_at': timezone.now() + timedelta(hours=1),
            'end_time': timezone.now() + timedelta(hours=2),
            'message_text': 'Test message',
            'filter_mobile_operator': 'XYZ',
            'filter_tag': 'test_tag',
        }

        # Создание рассылки через API
        response = self.client.post(url, data, format='json')

        # Проверка успешного создания
        assert response.status_code == status.HTTP_201_CREATED, response.data

        # Проверка, что объект был создан в базе данных
        assert MailingList.objects.filter(id=response.data['id']).exists()

        # Проверка данных объекта
        mailing_list = MailingList.objects.get(id=response.data['id'])
        assert mailing_list.message_text == data['message_text']
        assert mailing_list.filter_mobile_operator == data['filter_mobile_operator']
        assert mailing_list.filter_tag == data['filter_tag']

    # Обновленный тест для test_invalid_start_time
    def test_invalid_start_time(self):
        """
        Проверяем ввод неправильного времени старта
        """

        url = reverse('mailing-list-list')

        # тестовые данные с неправильным временем старта
        data = {
            'start_at': timezone.now() - timedelta(hours=1),
            'end_time': timezone.now() + timedelta(hours=2),
            'message_text': 'Test message',
            'filter_mobile_operator': 'XYZ',
            'filter_tag': 'test_tag',
        }

        # Попытка создать рассылку с неверным временем старта
        response = self.client.post(url, data, format='json')

        # Проверка, что запрос вернул код ошибки 400
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        # Проверка, что в ответе есть сообщение об ошибке
        assert 'Рассылка не может стартовать раньше текущего момента' in str(response.data['start_at'])


@pytest.mark.django_db
class TestMessageAPI(APITestCase):

    def test_create_message(self):
        """
        Создание рассылки
        """
        mailing_list = MailingList.objects.create(
            start_at='2022-01-01T12:00:00Z',
            message_text='Test message',
            filter_mobile_operator='XYZ',
            filter_tag='test_tag',
            end_time='2022-01-02T12:00:00Z',
        )
        client = Client.objects.create(phone_number='79123456789', mobile_operator_code='XYZ', tag='test_tag', timezone='UTC')

        url = reverse('message-list')
        data = {
            'status': 'sent',
            'mailing_list_id': mailing_list.id,
            'client_id': client.id,
        }
        response = self.client.post(url, data, format='json')
        assert response.status_code == status.HTTP_201_CREATED
        assert Message.objects.filter(status='sent').exists()

    def test_invalid_status(self):
        """Создание рассылки с неправильным статусом
        """
        
        mailing_list = MailingList.objects.create(
            start_at='2022-01-01T12:00:00Z',
            message_text='Test message',
            filter_mobile_operator='XYZ',
            filter_tag='test_tag',
            end_time='2022-01-02T12:00:00Z',
        )
        client = Client.objects.create(phone_number='79123456789', mobile_operator_code='XYZ', tag='test_tag', timezone='UTC')

        url = reverse('message-list')
        data = {
            'status': 'invalid_status',
            'mailing_list_id': mailing_list.id,
            'client_id': client.id,
        }
        response = self.client.post(url, data, format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert 'Invalid status'
