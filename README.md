# Сервис для управления рассылками (пример реализации)

Перед вами сервис управления рассылками API, администрирования и получения статистики.

## Запуск

Перед запуском вам необходимо создать свой собственный файл **.env/**. В корне выполните команду:

```bash
touch .env
```

Затем через любой текстовый редактор (vscode, vim, nano, etc) заполните его переменными (подставьте свои данные):

```code
SECRET_KEY=
DEBUG=
POSTGRES_NAME=testtask
POSTGRES_USER=youruser
POSTGRES_PASSWORD=yourpassword
POSTGRES_HOST=host
POSTGRES_PORT=port
ALLOWED_HOSTS=
DATABASE_URL=psql://youruser:yourpassword@host:port/testtask
JWT=Jwttoken
```

(предлопагаем, что Docker и Docker-Compose у вас есть):

Запускаем приложение командой 

```bash
docker-compose up --build
```

Все миграции пройдут автоматически (если все переменные заданы верно)

## Техническое задание
Необходимо реализовать методы создания новой рассылки, просмотра созданных и получения статистики по выполненным рассылкам

Реализовать сам сервис отправки уведомлений на внешнее API (токен находится в .env).

## Дополнительно

- Есть несколоько тестов (в рамках заданного времени - не на все)
- Есть docker-compose
- Есть подвязка к Prometeus (простейшие метрики)
- Отправка идет через Celery (по времени)
- Есть Swagger (описание минимальное, так как полноценная документация тоже требует времени)
- Если удаленный сервис недоступен, таск откладывается на 15 минут
- Есть некоторые логи (в тасках и отдельно для контейнера postgresql)