FROM python:3.9.13

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /

COPY . /

RUN pip install --no-cache-dir -r requirements.txt

RUN chmod +x ./entrypoint.sh

CMD ["./entrypoint.sh"]