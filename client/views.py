from django.shortcuts import render

from rest_framework import generics, viewsets
from client.serializers import ClientSerializer
from client.models import Client

class ClientListViewSet(viewsets.ModelViewSet):
    """
    Эндпойнт для работы с данными клиента
    """
    
    serializer_class = ClientSerializer
    queryset = Client.objects.all()