import pytest

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from .models import Client
from .serializers import ClientSerializer

@pytest.mark.django_db
class TestClientAPI(APITestCase):

    def test_create_client(self):
        """
        Проверяем нормальный ввод данных
        """
        
        url = reverse('client-list')

        # тестовые данные
        data = {
            'phone_number': '79123456789',
            'mobile_operator_code': 'XYZ',
            'tag': 'clie',
            'timezone': 'UTC',
        }

        # Создание клиента через API
        response = self.client.post(url, data, format='json')

        # Проверка успешного создания
        assert response.status_code == status.HTTP_201_CREATED, response.data

        # Проверка, что объект был создан в базе данных
        assert Client.objects.filter(phone_number=data['phone_number']).exists()

        # Проверка данных объекта
        client = Client.objects.get(phone_number=data['phone_number'])
        assert client.mobile_operator_code == data['mobile_operator_code']
        assert client.tag == data['tag']
        assert client.timezone == data['timezone']

    def test_invalid_phone_number(self):
        """
        Проверяем ввод неправильного номера телефона
        """
        url = reverse('client-list')

        # тестовые данные с неправильным номером
        data = {
            'phone_number': '12345', 
            'mobile_operator_code': 'XYZ',
            'tag': 'some_tag',
            'timezone': 'UTC',
        }

        # Попытка создать клиента с неверным форматом номера телефона
        response = self.client.post(url, data, format='json')

        # Проверка, что запрос вернул код ошибки 400
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        # Проверка, что в ответе есть сообщение об ошибке
        assert 'Номер телефона должен быть в формате 7XXXXXXXXXX' in response.data['phone_number']
