from django.db import models
from django_prometheus.models import ExportModelOperationsMixin

# уникальный id клиента
# номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)
# код мобильного оператора
# тег (произвольная метка)
# часовой пояс

class Client(ExportModelOperationsMixin('client'), models.Model):
    phone_number = models.CharField('Номер телефона', max_length=11, unique=True)
    mobile_operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=120)
    timezone = models.CharField(max_length=50)

    def __str__(self):
        return self.phone_number