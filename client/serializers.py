import re

from rest_framework import serializers
from client.models import Client


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'phone_number', 'mobile_operator_code', 'tag', 'timezone']

    def validate_phone_number(self, value):
        if not re.match(r'^7\d{10}$', value):
            raise serializers.ValidationError('Номер телефона должен быть в формате 7XXXXXXXXXX')
        return value