from django.urls import path, include

from rest_framework.routers import DefaultRouter
from client.views import ClientListViewSet

router = DefaultRouter()
router.register(r'client', ClientListViewSet, basename='client')

urlpatterns = [
    path('client-list/', ClientListViewSet.as_view({'get': 'list', 'post': 'create'}), name="client-list"),
]

urlpatterns += router.urls